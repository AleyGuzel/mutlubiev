﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mutlubiev.Common.Requests
{
    public class ApplicantRequest
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public char Gender { get; set; }
        public string DistrictId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mutlubiev.Common.Response
{
    public class ServiceResponse<T>
    {
        public bool HasExceptionError { get; set; } = true;



        public T Entity { get; set; }

        public int Count { get; set; }

        public bool Succeeded => !HasExceptionError && StatusCode >= 200 && StatusCode < 400;
            
     
        public String Message { get; set; } = string.Empty;

        public int StatusCode { get; set; } = 200;

    }
}

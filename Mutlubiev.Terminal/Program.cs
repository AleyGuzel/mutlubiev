﻿using Mutlubiev.Core.Context;
using Mutlubiev.Core.Entity.Category;
using System;
using System.Collections.Generic;

namespace Mutlubiev.Terminal
{
    class Program
    {
        static void Main(string[] args)
        {

            using (MutlubievContext context = new MutlubievContext())
            {

                //hizmete göre custom yorum cekildiği varsayıldı.
                
                var istanbul = new City()
                {
                    Name = "İstanbul",
                    Slug = "istanbul",
                  
                };
                context.Cities.Add(istanbul);

                var besiktas = new District()
                {
                    Name = "Beşiktaş",
                    Slug = "besiktas",
                    CityId = istanbul.Id
                };
                context.Districts.Add(besiktas);
                var kadikoy = new District()
                {
                    Name = "Kadıköy",
                    Slug = "kadikoy",
                    CityId = istanbul.Id

                };
                context.Districts.Add(kadikoy);
                var sisli = new District()
                {
                    Name = "Şişli",
                    Slug = "sisli",
                    CityId = istanbul.Id

                };
                context.Districts.Add(sisli);
                var ankara = new City()
                {
                    Name = "Ankara",
                    Slug = "ankara",
                };
                context.Cities.Add(ankara);
                var altindag = new District()
                {
                    Name = "Altındağ",
                    Slug = "altindag",
                    CityId = ankara.Id

                };
                context.Districts.Add(altindag);
                var cankaya = new District()
                {
                    Name = "Çankaya",
                    Slug = "cankaya",
                    CityId = ankara.Id

                };
                context.Districts.Add(cankaya);
                var etimesgut = new District()
                {
                    Name = "Etimesgut",
                    Slug = "etimesgut",
                    CityId = ankara.Id

                };
                context.Districts.Add(etimesgut);



                var evTemizligi = new Category()
                {
                    Name = "Ev Temizliği",
                    GoogleTitle = "Profesyonel Ev Temizliği Hizmeti Mutlubiev'de",
                    GoogleDescription = "Pırıl Pırıl, Mutlubiev. Ev Temizliğini Bize Bırak, Zamanın Sana Kalsın.  Profesyonel Hizmet ve Güvenilir Çalışanlarımız İle 7 Gün Hizmetinizdeyiz!",
                    Slug = "ev-temizligi",
                    ActionButtonText = "Temizlik İçin Tıkla!",
                    IsPopularCategory = true,
                    
                    CategoryDistricts = new List<CategoryDistrict>
                    {
                        new CategoryDistrict
                        {
                            DistrictID = besiktas.Id,
                            IsPopular = true,
                            Slug = "besiktas-ev-temizligi"
                        },
                        new CategoryDistrict
                        {
                            DistrictID = sisli.Id,
                            IsPopular = true,
                            Slug = "sisli-ev-temizligi"
                        },
                        new CategoryDistrict
                        {
                            DistrictID = cankaya.Id,
                            IsPopular = false,
                            Slug = "cankaya-ev-temizligi"
                        },
                        new CategoryDistrict
                        {
                            DistrictID = etimesgut.Id,
                            IsPopular = false,
                            Slug = "etimesgut-ev-temizligi"
                        }


                    },

                    SubCategories = new List<SubCategory>
                    {
                        new SubCategory() {
                            Name = "Mutfak Temizliği",
                            IconSrc = "/images/1-Mutfak_Temizliği.svg",
                            SubCategoryAbilities = new List<SubCategoryAbility>
                            {
                                new SubCategoryAbility
                                {
                                    Description = "Çöpler toplanır,boşaltılır ve yeni çöp torbalarının konulur."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Mutfak dolapları dıştan temizlenir ve erişilebilir üst alanların tozları alınır."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Bulaşıklar toparlanır ve bulaşık makinesine yerleştirilir, gerekirse yıkanır."
                                }
                            }
                        },
                        new SubCategory() { 
                            Name = "Banyo Temizliği", 
                            IconSrc = "/images/2-Banyo_Temizliği.svg",
                            SubCategoryAbilities = new List<SubCategoryAbility>
                            {
                                new SubCategoryAbility
                                {
                                    Description = "Çöpler toplanır,boşaltılır ve yeni çöp torbaları konulur."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Lavabo, musluklar ve tezgah fayans temizliği yapılır."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Dolaplar dıştan silinir ve dolap altındaki alan ile tesisat temizlenir."
                                }

                            }
                        },
                        new SubCategory() { 
                            Name = "Yatak Odası Temizliği", 
                            IconSrc = "/images/3-Yatak_Odası_Temizliği.svg",
                            SubCategoryAbilities = new List<SubCategoryAbility>
                            {
                                new SubCategoryAbility
                                {
                                    Description = "Mobilyaların yüzeyleri, erişilebilir alt ve üst alanların tozu alınarak temizlenir."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Ortada bulunan kıyafetler katlanarak genel düzenleme yapılır."
                                },
                                new SubCategoryAbility
                                {
                                    Description = "Kirişler ve bağlantı yerleri temizlenir."
                                }

                            }
                        },
                    },
                    NonIncludeAbilities = new List<NonIncludeAbility> {
                        new NonIncludeAbility() {
                            Description = "Camları dışarıdan temizlemek.",
                            IconSrc = "/images/pencere.svg",
                        },
                        new NonIncludeAbility() {
                            Description = "Ağırlığı 20 kg'den fazla eşyaları taşımak.",
                            IconSrc = "/images/pencere.svg" },
                        new NonIncludeAbility() {
                            Description = "Normalde ulaşılamayacak tehlikeli yerleri makineyle silmek.",
                            IconSrc = "/images/balkon.svg" },

                    },
                    Comments = new List<Comment>
                    {
                        new Comment
                        {
                            Description = "Mutlubiev'le evini ve kendini mutlu etmek %100! Çok kibar oldukları kadar, işlerinde de oldukça titizler! O kadar çok iş vardı ki çok kısa sürede her şeyi toparladılar ve temizlediler! Şimdi bu mis kokuyla mutlu mesut tadını çıkarıyorum! Hep gel sen mutlubiev!",
                            User = new User
                            {
                                Name = "Bahar",
                                LastName = "Kara"
                            }
                        },
                        new Comment
                        {
                            Description = "Çok sevdiği evine istediği ilgiyi gösteremediği için üzülen biriyseniz benim gibi epey mutlu olabilirsiniz. Kimsenin iş yaparken başında durmak zorunda da değilsiniz, oldukça titiz ve güvenilir şekilde evinizi temizliyorlar. Buzdolabı temizliği, dolap düzeni falan da çok iyi şeyler bence.",
                            User = new User
                            {
                                Name = "Beyza",
                                LastName = "Demir"


                            }
                        },
                        new Comment
                        {
                            Description = "Organize bir ekip çalışmasıyla hızlı ve titiz bir hizmet sunduğunuz icin cok teşekkürler!",
                            User = new User
                            {
                                Name = "Leyla",
                                LastName = "Cansever"

                            }
                        }
                    },
                    Faqs = new List<Faq>()
                    {
                        new Faq
                        {
                            Question ="Temizlik boyunca evde olmalı mıyım?",
                            Answer="Kesinlikle buna gerek yok! Sipariş adımlarında anahtarını bırakacağın yeri belirtebilirsin. Temizlik profesyonelimiz belirttiğin yerden anahtarı alıp bırakabilir. Hizmet süresince evde olmak istersen bizim için hiç sorun yok!"

                        },
                        new Faq
                        {
                            Question ="Nasıl bedava hizmet kazanırım?",
                            Answer="Mutlubiev’i arkadaşlarına önererek bedava hizmete ulaşman çok kolay! Senin referansınla hizmet alan her arkadaşın için 40 TL değerinde Mutlu Puan kazanabilir ve bunu dilediğin miktarda, dilediğin hizmette kullanabilirsin!"

                        },
                        new Faq
                        {
                            Question ="Hizmet süreleri nedir?",
                            Answer="Mutlubiev profesyonellerinin kesin bir hizmet süresi bulunmamaktadır. Mutlubiev kalitesinde bir ev temizliğini ve seçtiğin ek hizmetleri yerine getirmeden temizlik profesyonelimiz evinden çıkmayacaktır."

                        }

                    }
                };
                context.Categories.Add(evTemizligi);
                var ofisTemizligi = new Category()
                {
                    Name = "Ofis Temizliği",
                    GoogleTitle = "Profesyonel Ofis Temizliği Hizmeti Mutlubiev'de - mutlubiev.come",
                    GoogleDescription = "Mutlubiofis Mümkün! Ofis Temizliğini Aklından Çıkarmak İçin Hemen Fiyat Teklifi Al! Profesyonel Hizmet ve Güvenilir Çalışanlarımız İle 7 Gün Hizmetinizdeyiz!",
                    Slug = "ofis-temizligi",
                    ActionButtonText = "Fiyat Teklifi Al",
                    IsPopularCategory = true,

                    SubCategories = new List<SubCategory>
                    {
                        new SubCategory() {
                            Name = "Mobilya Temizliği",
                            IconSrc = "/images/4-Mobilya.svg",
                        },
                        new SubCategory() {
                            Name = "Genel Düzen",
                            IconSrc = "/images/5-Genel_Düzen.svg",
                        },
                        new SubCategory() {
                            Name = "Zemin Temizliği",
                            IconSrc = "/images/6-Zenin_Temizliği.svg",
                        },
                    },
                    Faqs = new List<Faq>()
                    {
                        new Faq
                        {
                            Question ="%100 Mutluluk Politikası nedir?",
                            Answer="Hizmet aldıktan sonra 72 saat içerisinde hizmetle ilgili herhangi bir şikayetin varsa bize bildirebilirsin! Hizmetin kalitesiyle ilgili bir sorun olduğu takdirde; sana şikayetinle orantılı olarak yeni bir  hizmeti programlayabiliriz. Biz buna %100 Mutluluk Politikası diyoruz!"
                        },

                    }
                };
                context.Categories.Add(ofisTemizligi);
                var mobilyaMontaj = new Category()
                {
                    Name = "Mobilya Montajı",
                    GoogleTitle = "Mobilya Montajı Hizmeti Mutlubiev'de - mutlubiev.com",
                    GoogleDescription = "Mobilya Montajı Hizmeti Mutlubiev' de. Mobilyanın Kurulumunu Bize Bırak, Zamanın Sana Kalsın. Profesyonel Hizmet ve Güvenilir Çalışanlarımız İle 7 Gün Hizmetinizdeyiz! ",
                    Slug = "mobilya-montaji",
                    ActionButtonText = "Hemen Hizmet Oluştur",
                    IsPopularCategory = false,

                    Comments = new List<Comment>
                    {
                        new Comment
                        {
                            Description = "Çok başarılı! Yıllardır aradığım servis, bu kadar kolay olacağını hiç beklemiyordum. Evi istediğim gibi dağıtabiliyorum!",
                            User = new User
                            {
                                Name = "Kaan",
                                LastName = "Başar"
                            }
                        },
                        new Comment
                        {
                            Description = "Evimde düzenlediğim partiden sonra nasıl temizlenecek bu ev derken tesadüf eseri mutlubiev’le karşılaştım denemekten bir şey kaybetmem düşüncesiyle ilk temizliğimi yaptırdım ve inanılmaz memnun kaldım. Kesinlikle anne eli değmiş gibi evin her yerinden gelen miss gibi kokularla karşılaştım. Bundan sonra annemi yormama gerek yok nasılsa mutlubiev var. Teşekkürler",
                            User = new User
                            {
                                Name = "Emir",
                                LastName = "Ulu"


                            }
                        },
                        new Comment
                        {
                            Description = "Kendi sektöründekilere kıyasla her şeyiyle dört dörtlük iş çıkaran ve %100 güvenilir hizmet sağlayan mutlubiev’i herkese tavsiye ediyorum :)",
                            User = new User
                            {
                                Name = "Pınar",
                                LastName = "İnci"

                            }
                        }
                    },
                    Faqs = new List<Faq>()
                    {
                        new Faq
                        {
                            Question ="Mobilya Montaj Hizmetini Nasıl Alabilirim?",
                            Answer="7 gün 24 saat, 2 adımda internet sitemizden istediğin gün ve saat için mobilya montajı siparişini verebilir, bu zahmetli işten kurtulabilirsin."

                        },
                        new Faq
                        {
                            Question ="Mobilya Montaj Hizmetine Neler Dahil?",
                            Answer="Mobilya montaj ustalarımız ile seçtiğin büyüklükteki mobilyalarını istediğin gün ve saatte gelip montajını sağlıyoruz. Her türlü mobilya kurulumu ve demonte mobilya kurulumu gibi hizmetler konusuda da her zaman yanındayız."

                        },
                        new Faq
                        {
                            Question ="Mobilya Montaj Hizmeti İçin Neden Mutlubiev’i Tercih Etmeliyim?",
                            Answer="Sadece 2 saat içinde evine gelerek mobilyalarının montajını gerçekleştiriyoruz. Tecrübeli profesyonellerimizin adli sicil kayıtlarını ve referanslarını kontrol ediyoruz. Mobilyalarında oluşabilecek hasarlara karşı AXA Sigorta ile 250.000 TL’ye kadar sigorta güvencesi sağlıyoruz. Net fiyatlandırma politikamızla şeffaf bir süreç yarattık. %100 Mutluluk Politikası ile karşılaşabileceğin tüm sorunları çözmek için elimizden gelen her şeyi yapıyoruz."

                        }

                    }
                };
                context.Categories.Add(mobilyaMontaj);



             //   context.SaveChanges();
            }


            Console.ReadLine();
        }
    }
}

﻿using Mutlubiev.Common.Requests;
using Mutlubiev.Common.Response;
using Mutlubiev.Core.Entity.Category;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Business.Services
{
    public interface IAplicantService
    {
        Task<ServiceResponse<List<City>>> Get();
        Task<ServiceResponse<bool>> Post(ApplicantRequest request);


    }
}

﻿using Microsoft.EntityFrameworkCore;
using Mutlubiev.Common.Response;
using Mutlubiev.Core.Entity.Category;
using Mutlubiev.Core.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Business.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public async Task<ServiceResponse<Category>> Get(string slug)
        {
            ServiceResponse<Category> response = new ServiceResponse<Category>();
            if (String.IsNullOrEmpty(slug))
            {
                response.Message = "Slug can not be empty";
                return response;
            }
            var categoryRepo = _unitOfWork.GetRepository<Category>();

            response.Entity = categoryRepo.Get(predicate: x => x.Slug == slug && x.Status == true, include:
                            source => source
                            .Include(t => t.NonIncludeAbilities)
                            .Include(t => t.SubCategories)
                            .ThenInclude(t => t.SubCategoryAbilities)
                            .Include(t => t.Faqs)
                            .Include(t => t.Comments)
                            .Include(t => t.CategoryDistricts)
                            );


            response.HasExceptionError = false;
            return response;
        }

        public async Task<ServiceResponse<Category>> GetWithDistrict(string slug)
        {
            ServiceResponse<Category> response = new ServiceResponse<Category>();
            if (String.IsNullOrEmpty(slug))
            {
                response.Message = "Slug can not be empty";
                return response;
            }
            var categoryDisctrictRepo = _unitOfWork.GetRepository<CategoryDistrict>();

            var categoryDistrict = categoryDisctrictRepo.Get(disableTracking : false, predicate: x => x.Slug == slug && x.Status == true, include:
                            source => source
                             .Include(t => t.Category)
                            .ThenInclude(t => t.NonIncludeAbilities)
                             .Include(t => t.Category)
                            .ThenInclude(t => t.SubCategories)
                            .ThenInclude(t => t.SubCategoryAbilities)
                             .Include(t => t.Category)
                            .ThenInclude(t => t.Faqs)
                             .Include(t => t.Category)
                            .ThenInclude(t => t.Comments)
                             .Include(t => t.Category)
                            .ThenInclude(t => t.CategoryDistricts)
                            ); 

            response.Entity = categoryDistrict.Category;

            response.HasExceptionError = false;
            return response;
        }
    }
}

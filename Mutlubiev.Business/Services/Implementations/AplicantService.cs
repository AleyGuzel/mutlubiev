﻿using Microsoft.EntityFrameworkCore;
using Mutlubiev.Common.Requests;
using Mutlubiev.Common.Response;
using Mutlubiev.Core.Entity;
using Mutlubiev.Core.Entity.Category;
using Mutlubiev.Core.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Business.Services.Implementations
{
    public class AplicantService : IAplicantService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AplicantService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

      
        public async Task<ServiceResponse<List<City>>> Get()
        {
            ServiceResponse<List<City>> response = new ServiceResponse<List<City>>();

            var cityRepo = _unitOfWork.GetRepository<City>();

            response.Entity = cityRepo.GetList(predicate: x => x.Status == true, include:
                            source => source
                            .Include(t => t.Districts)
                            ).ToList();

            response.HasExceptionError = false;
            return response;
        }

        public async Task<ServiceResponse<bool>> Post(ApplicantRequest request)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();

            if (string.IsNullOrEmpty(request.FullName) ||
                string.IsNullOrEmpty(request.PhoneNumber) ||
                string.IsNullOrEmpty(request.DistrictId)
                )
            {
                response.Message = "Gerekli yerleri doldurunuz.";
                return response;
            }

            var applicantRepo = _unitOfWork.GetRepository<Applicant>();
            var disrictRepo = _unitOfWork.GetRepository<District>();
            var discrict = disrictRepo.FirstOrDefault(predicate: x => x.Id == new Guid(request.DistrictId));
            if (discrict == null)
            {
                response.Message = "Bir sorun oluştu";
                return response;
            }
            Applicant applicant = new Applicant();

            applicant.FullName = request.FullName;
            applicant.PhoneNumber = request.PhoneNumber;
            applicant.Gender = request.Gender;
            applicant.DistrictId = discrict.Id;

            applicantRepo.Add(applicant);
            await _unitOfWork.CompleteAsnyc();

            response.HasExceptionError = false;
            return response;

        }

    }
}

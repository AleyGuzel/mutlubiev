﻿using Mutlubiev.Common.Response;
using Mutlubiev.Core.Entity.Category;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Business.Services
{
    public interface ICategoryService
    {
        Task<ServiceResponse<Category>> Get(string slug);
        Task<ServiceResponse<Category>> GetWithDistrict(string slug);
    }
}

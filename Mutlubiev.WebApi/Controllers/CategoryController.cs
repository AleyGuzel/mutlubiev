﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mutlubiev.Business.Services;

namespace Mutlubiev.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [HttpGet()]
        [Route("get/{slug}")]
        public async Task<IActionResult> Get(string slug)
        {
            return Ok(await _categoryService.Get(slug));
        }

        [HttpGet()]
        [Route("get-with-district/{slug}")]
        public async Task<IActionResult> GetDiscrict(string slug)
        {
            return Ok(await _categoryService.GetWithDistrict(slug));
        }
    }
}

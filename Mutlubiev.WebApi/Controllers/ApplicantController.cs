﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mutlubiev.Business.Services;
using Mutlubiev.Common.Requests;

namespace Mutlubiev.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicantController : ControllerBase
    {
        private readonly IAplicantService _aplicantService;
        public ApplicantController(IAplicantService aplicantService)
        {
            _aplicantService = aplicantService;
        }

        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _aplicantService.Get());
        }

        [HttpPost]
        [Route("post")]
        public async Task<IActionResult> PostApplicant( ApplicantRequest request)
        {
            return Ok(await _aplicantService.Post(request));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("category_district")]
    public class CategoryDistrict : BaseEntity
    {
        [ForeignKey("Category")]
        [Column("category_id")]
        public Guid CategoryID { get; set; }

        [ForeignKey("District")]
        [Column("district_id")]
        public Guid DistrictID { get; set; }
        [Column("is_popular")]
        public bool IsPopular { get; set; }
        [Column("slug")]
        public string Slug { get; set; }
        public virtual Category Category { get; set; }
        public virtual District District { get; set; }


    }
}

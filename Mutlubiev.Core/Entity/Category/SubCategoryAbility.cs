﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("subcategory_ability")]
    public class SubCategoryAbility : BaseEntity
    {
        [Column("description")]
        public string Description { get; set; }

        [ForeignKey("SubCategory")]
        [Column("sub_category_id")]
        public Guid SubCategoryID { get; set; }
        public virtual SubCategory SubCategory { get; set; }
    }
}

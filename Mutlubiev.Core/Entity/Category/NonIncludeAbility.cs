﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("noninclude_ability")]
    public class NonIncludeAbility :BaseEntity
    {
        [Column("description")]
        public string Description { get; set; }
        [Column("icon_src")]
        public string IconSrc { get; set; }

        [ForeignKey("Category")]
        [Column("category_id")]
        public Guid CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}

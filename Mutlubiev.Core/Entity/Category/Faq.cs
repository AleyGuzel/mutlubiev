﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("Faq")]
    public class Faq : BaseEntity
    {
        [Column("question")]
        public string Question { get; set; }
        [Column("answer")]
        public string Answer { get; set; }

        [ForeignKey("Category")]
        [Column("category_id")]
        public Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("category")]
    public class Category :BaseEntity
    {
        
        [Column("name")]
        public string Name { get; set; }
        
        [Column("google_title")]
        public string GoogleTitle { get; set; }

        [Column("google_description")]
        public string GoogleDescription { get; set; }

        [Column("slug")]
        public string Slug { get; set; }

        [Column("action_button_text")]
        public string ActionButtonText { get; set; }

        [Column("is_popular_category")]
        public bool IsPopularCategory { get; set; }
        public virtual List<NonIncludeAbility> NonIncludeAbilities { get; set; }
        public virtual List<SubCategory> SubCategories { get; set; }
        public virtual List<Faq> Faqs { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual List<CategoryDistrict> CategoryDistricts { get; set; }
    }
}

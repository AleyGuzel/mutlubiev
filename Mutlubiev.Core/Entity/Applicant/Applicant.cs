﻿using Mutlubiev.Core.Entity.Category;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity
{
    [Table("applicant")]
    public class Applicant : BaseEntity
    {
        [Column("full_name")]
        public string FullName { get; set; }
        [Column("phone_number")]
        public string PhoneNumber { get; set; }
        [Column("gender")]
        public char Gender { get; set; }
        [Column("is_approved")]
        public bool IsApproved { get; set; }


        [ForeignKey("District")]
        [Column("district_id")]
        public Guid DistrictId { get; set; }




        public virtual District District { get; set; }


    }
}

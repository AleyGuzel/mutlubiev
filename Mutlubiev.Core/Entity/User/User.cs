﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("user")]
    public class User :BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        public virtual List<Comment> Comments { get; set; }

    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mutlubiev.Core.Entity
{
    public abstract class BaseEntity : IBaseEntity
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Column("modified_at")]
        public DateTime ModifiedAt { get; set; }

        [Column("status")]
        public bool Status { get; set; } = true;


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("city")]
    public class City : BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("slug")]
        public string Slug { get; set; }
        public virtual List<District> Districts { get; set; }
    }
}


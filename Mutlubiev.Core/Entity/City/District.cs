﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("district")]
    public class District :BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("slug")]
        public string Slug { get; set; }

        [ForeignKey("City")]
        [Column("city_id")]
        public Guid CityId { get; set; }

        public virtual City City { get; set; }

        public virtual List<CategoryDistrict> CategoryDistricts { get; set; }
        public virtual List<Applicant> Applicants { get; set; }

    }
}

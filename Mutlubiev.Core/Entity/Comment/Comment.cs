﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Mutlubiev.Core.Entity.Category
{
    [Table("comment")]
    public class Comment : BaseEntity
    {
        [Column("description")]
        public string Description { get; set; }
        [Column("star")]
        public decimal Star { get; set; }

        [ForeignKey("Category")]
        [Column("category_id")]
        public Guid CategoryId { get; set; }

        [ForeignKey("User")]
        [Column("user_id")]
        public Guid UserId { get; set; }

        public virtual Category Category { get; set; }
        public virtual User User { get; set; }



    }
}

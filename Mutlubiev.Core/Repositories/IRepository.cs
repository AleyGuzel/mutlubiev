﻿using Microsoft.EntityFrameworkCore.Query;
using Mutlubiev.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Mutlubiev.Core.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        T Get(Guid id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);

        T FirstOrDefault(Expression<Func<T, bool>> predicate);

        T Get(Expression<Func<T, bool>> predicate = null, //default değerlerini verdik çünkü articles controllerda kullanırken predicate: diye hangi parametreyi verdiğimi söyledim ama istersen sadece invlude: da diyebilirdim defaultları olmasa sırayla ve değer vererek sorgulamak zorunda kalırdım. Ruby'de de benzer bir kullanım vardı.
                                                  Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                                  Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                                                  bool disableTracking = true,
                                                  bool ignoreQueryFilters = false);


        IQueryable<T> GetList(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            bool disableTracking = true,
            bool ignoreQueryFilters = false);
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mutlubiev.Core.Migrations
{
    public partial class addedslugcolumntocategorydictrict : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "category_district",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "slug",
                table: "category_district");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mutlubiev.Core.Migrations
{
    public partial class status_column_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_applicant_city_CityId",
                table: "applicant");

            migrationBuilder.DropIndex(
                name: "IX_applicant_CityId",
                table: "applicant");

            migrationBuilder.DropColumn(
                name: "CityId",
                table: "applicant");

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "user",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "subcategory_ability",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "sub _category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "noninclude_ability",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "Faq",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "district",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "comment",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "city",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "category_district",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "applicant",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "status",
                table: "user");

            migrationBuilder.DropColumn(
                name: "status",
                table: "subcategory_ability");

            migrationBuilder.DropColumn(
                name: "status",
                table: "sub _category");

            migrationBuilder.DropColumn(
                name: "status",
                table: "noninclude_ability");

            migrationBuilder.DropColumn(
                name: "status",
                table: "Faq");

            migrationBuilder.DropColumn(
                name: "status",
                table: "district");

            migrationBuilder.DropColumn(
                name: "status",
                table: "comment");

            migrationBuilder.DropColumn(
                name: "status",
                table: "city");

            migrationBuilder.DropColumn(
                name: "status",
                table: "category_district");

            migrationBuilder.DropColumn(
                name: "status",
                table: "category");

            migrationBuilder.DropColumn(
                name: "status",
                table: "applicant");

            migrationBuilder.AddColumn<Guid>(
                name: "CityId",
                table: "applicant",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_applicant_CityId",
                table: "applicant",
                column: "CityId");

            migrationBuilder.AddForeignKey(
                name: "FK_applicant_city_CityId",
                table: "applicant",
                column: "CityId",
                principalTable: "city",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mutlubiev.Core.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "category",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    google_title = table.Column<string>(nullable: true),
                    google_description = table.Column<string>(nullable: true),
                    slug = table.Column<string>(nullable: true),
                    action_button_text = table.Column<string>(nullable: true),
                    is_popular_category = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "city",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    slug = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Faq",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    question = table.Column<string>(nullable: true),
                    answer = table.Column<string>(nullable: true),
                    category_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faq", x => x.id);
                    table.ForeignKey(
                        name: "FK_Faq_category_category_id",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "noninclude_ability",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    icon_src = table.Column<string>(nullable: true),
                    category_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_noninclude_ability", x => x.id);
                    table.ForeignKey(
                        name: "FK_noninclude_ability_category_category_id",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "sub _category",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    icon_src = table.Column<string>(nullable: true),
                    category_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub _category", x => x.id);
                    table.ForeignKey(
                        name: "FK_sub _category_category_category_id",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "district",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    slug = table.Column<string>(nullable: true),
                    city_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_district", x => x.id);
                    table.ForeignKey(
                        name: "FK_district_city_city_id",
                        column: x => x.city_id,
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "comment",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    star = table.Column<decimal>(nullable: false),
                    category_id = table.Column<Guid>(nullable: false),
                    user_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_comment", x => x.id);
                    table.ForeignKey(
                        name: "FK_comment_category_category_id",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_comment_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "subcategory_ability",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    sub_category_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subcategory_ability", x => x.id);
                    table.ForeignKey(
                        name: "FK_subcategory_ability_sub _category_sub_category_id",
                        column: x => x.sub_category_id,
                        principalTable: "sub _category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "applicant",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    full_name = table.Column<string>(nullable: true),
                    phone_number = table.Column<string>(nullable: true),
                    gender = table.Column<string>(nullable: false),
                    is_approved = table.Column<bool>(nullable: false),
                    district_id = table.Column<Guid>(nullable: false),
                    CityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_applicant", x => x.id);
                    table.ForeignKey(
                        name: "FK_applicant_city_CityId",
                        column: x => x.CityId,
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_applicant_district_district_id",
                        column: x => x.district_id,
                        principalTable: "district",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "category_district",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    modified_at = table.Column<DateTime>(nullable: false),
                    category_id = table.Column<Guid>(nullable: false),
                    district_id = table.Column<Guid>(nullable: false),
                    is_popular = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_district", x => x.id);
                    table.ForeignKey(
                        name: "FK_category_district_category_category_id",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_category_district_district_district_id",
                        column: x => x.district_id,
                        principalTable: "district",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_applicant_CityId",
                table: "applicant",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_applicant_district_id",
                table: "applicant",
                column: "district_id");

            migrationBuilder.CreateIndex(
                name: "IX_category_district_category_id",
                table: "category_district",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_category_district_district_id",
                table: "category_district",
                column: "district_id");

            migrationBuilder.CreateIndex(
                name: "IX_comment_category_id",
                table: "comment",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_comment_user_id",
                table: "comment",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_district_city_id",
                table: "district",
                column: "city_id");

            migrationBuilder.CreateIndex(
                name: "IX_Faq_category_id",
                table: "Faq",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_noninclude_ability_category_id",
                table: "noninclude_ability",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_sub _category_category_id",
                table: "sub _category",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_subcategory_ability_sub_category_id",
                table: "subcategory_ability",
                column: "sub_category_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "applicant");

            migrationBuilder.DropTable(
                name: "category_district");

            migrationBuilder.DropTable(
                name: "comment");

            migrationBuilder.DropTable(
                name: "Faq");

            migrationBuilder.DropTable(
                name: "noninclude_ability");

            migrationBuilder.DropTable(
                name: "subcategory_ability");

            migrationBuilder.DropTable(
                name: "district");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "sub _category");

            migrationBuilder.DropTable(
                name: "city");

            migrationBuilder.DropTable(
                name: "category");
        }
    }
}

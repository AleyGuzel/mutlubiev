﻿using Microsoft.EntityFrameworkCore;
using Mutlubiev.Core.Entity;
using Mutlubiev.Core.Entity.Category;

namespace Mutlubiev.Core.Context
{
    public class MutlubievContext : DbContext
    {
        
         public MutlubievContext(DbContextOptions<MutlubievContext> options): base(options)
         {

         }

        public MutlubievContext() : base()
        {

        }

        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryDistrict> CategoryDistricts { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<NonIncludeAbility> NonIncludeAbilities { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<SubCategoryAbility> SubCategoryAbilities { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured==false)
            {
                optionsBuilder.UseSqlServer(@"Server=.;Database=Mutlubiev;Trusted_Connection=True;");
            }
            

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            

        }
    }
}

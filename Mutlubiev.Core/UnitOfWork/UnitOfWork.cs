﻿using Mutlubiev.Core.Context;
using Mutlubiev.Core.Entity;
using Mutlubiev.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Core.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MutlubievContext _context;
        public UnitOfWork(MutlubievContext context)
        {
            _context = context;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }


        public async Task<int> CompleteAsnyc()
        {
            return await _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

﻿using Mutlubiev.Core.Entity;
using Mutlubiev.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mutlubiev.Core.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        int Complete();

        Task<int> CompleteAsnyc();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;
    }
}
